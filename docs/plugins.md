
이 튜토리얼의 앞부분에서 몇 가지 유용한 `pytest` 플러그인에 대해 살펴보았다. 이 장에서는 `pytest-randomly`와 같은 유틸리티 플러그인에서부터 Django와 같은 라이브러리에 특정된 플러그인에 이르기까지 플러그인들에 대해 자세히 살펴보겠다.

## `pytest-randomly`
종종 테스트의 순서가 중요하지 않지만 코드베이스가 증가함에 따라 일부 테스트가 제대로 실행되지 않을 경우 실패하는 몇 가지 부작용이 발생할 수 있습니다.

`pytest-randomly`는 테스트를 랜덤 순서로 실행시킨다. `pytest`는 항상 실행하기 전에 찾을 수 있는 모든 테스트를 수집한다. `pytest-randomly`은 실행 전에 테스트 목록을 뒤섞어 놓기만 한다.

이는 특정 순서에 의존하는 테스트를 발견하는 좋은 방법이다. 즉, 다른 테스트에 대한 **상태 의존도(stateful dependency)**가 있다는 것을 의미한다. `pytest`에서 테스트 스위트을 처음부터 구축했다면 가능성이 매우 낮지만 이 문제는 `pytest`로 마이그레이션하는 테스트 스위트에서 발생할 가능성이 높다.

플러그인은 구성 설명에 시드 값을 출력한다. 이 값을 사용하여 문제를 해결할 때와 동일한 순서로 테스트를 실행할 수 있다.

## `pytest-cov`
테스트가 구현 코드의 어떤 부분을 수행하였는 지를 측정하려면 [coverage](https://coverage.readthedocs.io/) 패키지를 사용할 수 있다. `pytest-cov`는 커버리지를 통합하였다. `pytest --cov`를 실행하여 테스트 커버리지 보고서를 출력한다.

## `pytest-django`
`pytest-django`는 Django 테스트를 처리하는 데 유용한 몇 가지 fixture와 마크를 제공한다. 이 튜토리얼의 앞부분에서 django_db 마크를 보았을 것이다. `rf` fixture를 통해 Django의 [RequestFactory](https://docs.djangoproject.com/en/3.0/topics/testing/advanced/#django.test.RequestFactory) 인스턴스에 직접 액세스할 수 있다. `settings` fixture를 사용하면 Django 설정을 빠르게 설정하거나 재정의할 수 있다. 이러한 플러그인은 Django 테스트의 생산성을 크게 향상시킬 수 있다.

[Pytest에서 Django 모델에 테스트 fixture를 제공하는 방법](https://realpython.com/django-pytest-fixtures/)에 Django에서 `pytest`를 사용하는 방법에 대해 자세히 설명하고 있다.

## `pytest-bdd`
`pytest`는 기존 단위 테스트 범위를 넘어 사용할 수도 있다. [행동 기반 개발(behavior-driven deveopment(BDD))](https://en.wikipedia.org/wiki/Behavior-driven_development)은 가능한 사용자 행동 및 예상에 대한 통상의 언어로 설명을 작성하는 것을 권장하며, 이 설명을 사용하여 특정 기능을 구현할지 여부를 결정할 수 있다. [pytest-bdd](https://pytest-bdd.readthedocs.io/en/latest/)는 [Gherkin](http://docs.behat.org/en/v2.5/guides/1.gherkin.html)을 사용하여 코드에 대한 기능 테스트를 작성할 수 있도록 도와준다.

이 광범위한 서드파티 플러그인 목록을 통해 `pytest`에 사용할 수 있는 많은 플러그인들을 찾아 볼 수 있다.
