이 튜토리얼 페이지의 예제를 수행하여면 `pytest`를 설치해야 한다. 대부분의 [Python packages](https://realpython.com/python-modules-packages/)가 그렇듯이 [PyPI](https://realpython.com/pypi-publish-python-package/)에서 `pytest`를 사용할 수 있다. [pip](https://realpython.com/what-is-pip/)를 사용하여 [가상 환경](https://realpython.com/what-is-pip/)에 설치한다.

```shell
$ python -m venv venv
$ source venv/bin/activate
(venv) $ python -m pip install pytest
```

이제 `pytest` 명령을 설치 환경에서 사용할 수 있다.
