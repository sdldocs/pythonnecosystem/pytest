# Effective Python Testing With Pytest
[Effective Python Testing With Pytest](https://realpython.com/pytest-python-testing/)를 편역하였다.

## 목차

- [`pytest` 설치](install.md)
- [`pytest`의 유용성](useful.md)
- [고정(Fixtures): 상태 및 종속성 관리](fixtures.md)
- [마크(Marks): 테스트(tests) 분류](marks.md)
- [파라미터화(parameterization): 테스트 결합](parameterization.md)
- [Duration 보고서: 느린 테스트과의 싸움](reports.md)
- [유용한 `pytest` 플러그인](plugins.md)
- [마치며](remarks.md)


[코드 테스팅](https://realpython.com/python-testing/)에는 다양한 이점이 있다. 코드가 기대한 대로 작동하고 코드에 대한 변경사항이 퇴행되지 않도록 하여 코드의 신뢰도를 높인다. 테스트을 작성하고 유지하는 것은 쉽지 않은 작업이므로, 가능한 큰 수고없이 테스트를 수행할 수 있도록 필요한 도구를 활용해야 한다. pytest는 테스트 생산성을 높이는 데 사용할 수 있는 최고의 도구 중 하나일 것이다.

이 튜토리얼 페이지에서는 다음을 설명한다.

- pytest 사용의 **이점**
- 테스트가 **stateless** 상태인지 확인하는 방법
- 반복 시험을 보다 **쉽게** 만드는 방법
- 이름 또는 사용자 그룹별로 테스트의 **일부**를 실행하는 방법
- **재사용** 가능한 테스트 유틸리티를 만들고 유지하는 방법

