
컨텍스트를 구현 코드에서 테스트 코드로 전환할 때마다 약간의 [오버헤드](https://en.wikipedia.org/wiki/Overhead_(computing))가 발생한다. 처음부터 테스트가 느린 경우 오버헤드가 마찰과 좌절을 일으킬 수 있다.

앞에서 스위트을 실행할 때 마크를 사용하여 느린 테스트를 필터링하는 방법에 대해 설명하였지만, 어느 시점에 이르면 해당 테스트를 실행해야 한다. 테스트 속도를 향상시키려면 *어떤* 테스트가 가장 큰 개선 효과를 제공할 수 있는지 아는 것이 유용하다. `pytest`는 자동으로 사용자를 위해 시험 기간을 기록하고 실행 시간 상위 테스트를 보고할 수 있다.

`pytest` 명령에 `--durations` 옵션을 사용하여 테스트 결과에 기간 보고서(duration reposrt)를 생성한다. `--durations`는 정수 값 n을 입력받아 가장 느린 n개의 테스트를 출력한다. 테스트 보고서에 새로운 섹션이 포함된다.

```bash
(venv) $ pytest --durations=5
...
============================= slowest 5 durations =============================
3.03s call     test_code.py::test_request_read_timeout
1.07s call     test_code.py::test_request_connection_timeout
0.57s call     test_code.py::test_database_read

(2 durations < 0.005s hidden.  Use -vv to show these durations.)
=========================== short test summary info ===========================
...
```

duration 리포트에 출력되는 각 테스트는 전체 테스트 시간 중 평균 이상의 시간이 소요되기 때문에 속도를 높이기에 좋은 후보이다. 짧은 기간 테스트는 기본적으로 출력하지 않는다. 리포트에 설명된 대로 리포트의 다양성을 높이고 `-vv`를 `--duration`과 함께 전달하여 테스트를 출력할 수 있다.

일부 테스트에서는 테스트를 보잊 않도록 하는 설정의 오버헤드가 있을 수 있다. 앞에서 `django_db`로 표시된 첫 번째 테스트가 Django 테스트 데이터베이스 생성을 트리거하는 방법에 대해 설명하였다. duration 보고서는 데이터베이스 생성을 트리거한 테스트에서 데이터베이스를 설정하는 데 걸리는 시간을 반영하므로 오해의 소지가 있을 수 있다.

완전한 test coverage로 가는 중이다. 다음 장에서는 풍부한 `pytest` 플러그인 에코시스템의 일부인 몇몇 플러그인에 대하여 설명하려고 한다.
