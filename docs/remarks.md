
`pytest`는 테스트를 필터링하고 최적화할 수 있는 생산성 기능의 핵심 세트와 더불어 그 가치를 더욱 확장하는 유연한 플러그인 시스템을 지원한다. 대규모 레거시 `unittest` 스위트을 보유하고 있든, 새로운 프로젝트를 처음부터 시작하든, `pytest`는 여러 기능을 지원하고 있다.

이 튜토리얼에서 다음 사용 방법을 설명하였다.

- 테스트 종속성, 상태 및 재사용 가능한 기능을 다루기 위한 **fixture**
- 테스트를 분류하고 외부 리소스에 대한 액세스를 제한하는 **마크**
- 테스트간 중복 코드를 줄이기 위한 **파라미터화**
- 가장 느린 테스트를 찾기 위한 **duration**
- 다른 프레임워크 및 테스트 도구와 통합하기 위한 **플러그인**

`pytest`를 설치하고 시도해 보자. Happy Testing!

만약 `pytest`로 구축된 예제 프로젝트를 보고 싶다면, [building a hash table with TDD](https://realpython.com/python-hash-table/) 튜토리얼을 보세요. 이 튜토리얼은 `pytest`를 사용하는 속도를 높일 뿐만 아니라 해시 테이블을 마스터하는 데 도움이 될 것이다!
