예전에 Python 코드에 대한 단위 테스트를 작성한 적이 있다면 파이썬의 내장 `unitest` 모듈을 사용한 적이 있을 것이다. `unittest`는 테스트 제품군을 구축할 수 있는 확실한 기반을 제공하지만 몇 가지 단점이 있다.

많은 서드파티 테스트 프레임워크가 unitest의 일부 문제를 해결하려고 시도하고 있으며, `pytest`가 가장 보편적인 것 중 하나이다. `pytest`는 Python 코드를 테스트하기 위한 기능이 풍부한 플러그인 기반 에코시스템이다.

만약 여러분이 아직 `pytest`를 사용해 보지 않았다면, 한턱내세요! 그 철학과 특징은 여러분의 테스팅 경험을 더 생산적이고 즐겁게 만들어 줄 것이다. `pytest`를 사용하면 일반적인 작업에 필요한 코드가 줄어들며 다양한 시간 절약 명령과 플러그인을 통해 고급 작업을 수행할 수 있을 것이다. `unittest`를 포함하여 기존 테스트를 즉시 실행할 수도 있다.

대부분의 프레임워크와 마찬가지로 `pytest`를 처음 사용하기 시작할 때 의미가 있는 일부 개발 패턴은 테스트 슈트(test suite)가 증가함에 따라 문제를 일으킬 수 있다. 이 튜토리얼 페이지에서는 `pytest`가 테스트 증가에도 테스트를 효율적이고 효과적으로 유지하기 위해 제공하는 도구들을 이해하는 데 도움이 될 것이다.

## 적은 표준 문안 (Less Boilerplate)
대부분의 기능 테스트(functional test)는 Arranger-Act-Assert 모델을 따른다.

1. 시험을 위한 조건의 **Arrange** 또는 설정
2. 일부 함수 또는 메서드를 호출하여 **Act**
3. 일부 종료 조건이 참이라고 **Assert**

테스팅 프레임워크는 일반적으로 테스트 [assertions](https://realpython.com/python-assert-statement/)에 연결되므로 assertion이 실패할 때 정보를 제공할 수 있다. 예를 들어 `unittest`는 여러 가지 유용한 assertion 유틸리티를 즉시 제공한다. 그러나 작은 일련의 테스트에도 상당한 양의 [표준 패턴 코드(boilerplate code)](https://en.wikipedia.org/wiki/Boilerplate_code)가 필요하다.

프로젝트에서 `unittest`가 정확히 동작하는지 확인하기 위해 테스트 슈트를 작성을 상상해 보십시오. 항상 합격(pass)하는 테스트와 항상 불합격(fail)하는 테스트를 작성할 수 있다.

```python
# test_with_unittest.py

from unittest import TestCase

class TryTesting(TestCase):
    def test_always_passes(self):
        self.assertTrue(True)

    def test_always_fails(self):
        self.assertTrue(False)
```

그런 다음 `unittest`의 `discover` 옵션을 사용하여 명령에서 이러한 테스트를 실행할 수 있다.

```shell
(venv) $ python -m unittest discover

----------------------------------------------------------------------
Ran 0 tests in 0.000s

OK
```

예상대로 한 번의 테스트는 통과했고 한 번의 테스트는 실패했다. `unittest`가 효과가 있다는 것을 증명했지만, 무엇을 해야 했는지 살펴보자.

1. `unittest`에서 `TestCase` 클래스를 import한다.
2. `TestCase`의 [서브클래스](https://realpython.com/python3-object-oriented-programming/)인 `TryTesting`를 만든다.
3. 각각의 테스트를 위한 `TryTesting`에 메서드를 작성한다.
4. assertion을 만들기 위하여, `unittest.TestCase`로부터 `unittest.assert*`중 하나를 사용한다.

이는 작성해야 할 상당한 양의 코드이며, 테스트에 필요한 최소 코드이기 때문에 동일한 코드를 반복해서 작성하게 된다. 일반 함수와 Python의 `assert` 키워드를 직접 사용할 수 있게 함으로써 `pytest`는 이 워크플로우를 단순화한다.

```python
# test_with_pytest.py

def test_always_passes():
    assert True

def test_always_fails():
    assert False
```

import나 클래스를 사용하지 않는다. `test_` 접두사가 있는 함수를 포함하기만 하면 된다. `assert` 키워드를 사용할 수 있기 때문에 `unittest`에서 `self.assert*` 메서드를 모두 배우거나 기억할 필요가 없다. True로 예상하는 식(expression)을 작성하면 `pytest`가 이를 테스트한다.

`pytest`는 많은 표준 문안을 생략할 뿐만 아니라 훨씬 더 상세하고 읽기 쉬운 출력을 제공한다.

## 멋진 출력
프로젝트의 최상위 폴더에서 `pytest` 명령을 사용하여 테스트 슈트을 실행할 수 있다.

```shell
(venv) $ pytest
================================================================================= test session starts ==================================================================================
platform darwin -- Python 3.8.2, pytest-7.1.2, pluggy-1.0.0
rootdir: /Users/yjlee/MyProjects/SDLDocs/TestPytest
collected 4 items                                                                                                                                                                      

test_with-unitest.py F.                                                                                                                                                          [ 50%]
test_with_pytest.py .F                                                                                                                                                           [100%]

======================================================================================= FAILURES =======================================================================================
_____________________________________________________________________________ TryTesting.test_always_fails _____________________________________________________________________________

self = <test_with-unitest.TryTesting testMethod=test_always_fails>

    def test_always_fails(self):
>       self.assertTrue(False)
E       AssertionError: False is not true

test_with-unitest.py:10: AssertionError
__________________________________________________________________________________ test_always_fails ___________________________________________________________________________________

    def test_always_fails():
>       assert False
E       assert False

test_with_pytest.py:7: AssertionError
=============================================================================== short test summary info ================================================================================
FAILED test_with-unitest.py::TryTesting::test_always_fails - AssertionError: False is not true
FAILED test_with_pytest.py::test_always_fails - assert False
============================================================================= 2 failed, 2 passed in 0.09s ==============================================================================
```

`pytest`는 테스트 결과를 `unittest`와 다르게 출력하며 `test_with_unittest.py` 파일도 자동으로 포함되었다. 보고서에 다음을 출력한다.

1. 설치한 `Python`, `pytest` 및 `plugin`을 포함한 시스템 상태
2. `rootdir` 또는 구성과 테스트를 검색할 디렉토리
3. 수행자가 발견한 테스트 수

다음 항목은 출력의 첫 번째 섹션에 출력한다.

```shell
================================================================================= test session starts ==================================================================================
platform darwin -- Python 3.8.2, pytest-7.1.2, pluggy-1.0.0
rootdir: /Users/yjlee/MyProjects/SDLDocs/TestPytest
collected 4 items
```

그 다음 출력은 `unittest`와 유사하게 각 테스트의 상태를 나타낸다.

- **점(.)**은 테스트가 통과되었음을 의미한다.
- **F**는 테스트가 실패했음을 의미한다.
- **E**는 테스트에서 예기치 않은 예외가 발생했음을 의미한다.

특수 문자는 이름 옆에 표시되며 테스트 슈트의 전체 진행 상황은 오른쪽에 표시된다.

```shell
test_with-unitest.py F.                                                                                                                                                          [ 50%]
test_with_pytest.py .F                                                                                                                                                           [100%]
```

실패한 테스트의 경우 보고서에는 실패에 대한 자세한 설명이 나타난다. 이 예에서는 `assert False`가 항상 실패하기 때문에 테스트가 실패했다.

```shell
======================================================================================= FAILURES =======================================================================================
_____________________________________________________________________________ TryTesting.test_always_fails _____________________________________________________________________________

self = <test_with-unitest.TryTesting testMethod=test_always_fails>

    def test_always_fails(self):
>       self.assertTrue(False)
E       AssertionError: False is not true

test_with-unitest.py:10: AssertionError
__________________________________________________________________________________ test_always_fails ___________________________________________________________________________________

    def test_always_fails():
>       assert False
E       assert False

test_with_pytest.py:7: AssertionError
```

이 추가 출력은 디버깅할 때 매우 유용할 수 있다. 마지막으로 이 보고서는 테스트 슈트의 전체 상태를 출력한다.

```shell
=============================================================================== short test summary info ================================================================================
FAILED test_with-unitest.py::TryTesting::test_always_fails - AssertionError: False is not true
FAILED test_with_pytest.py::test_always_fails - assert False
============================================================================= 2 failed, 2 passed in 0.09s ==============================================================================
```

unitest와 비교하면 `pytest` 출력은 훨씬 더 유익한 정보를 얻을 수 있다.

다음 섹션에서는 `pytest`가 기존 `assert` 키워드를 어떻게 활용하는지에 대해 자세히 설명한다.

## 적은 학습
[assert](https://realpython.com/python-assert-statement/) 키워드를 사용할 수 있는 것 또한 강력하다. 이미 사용해 봤다면, 새로운 것이 아무것도 없다. 다음은 테스트 유형을 파악할 수 있는 몇 가지 assert 예제이다.

```python
# test_assert_examples.py

def test_uppercase():
    assert "loud noises".upper() == "LOUD NOISES"

def test_reversed():
    assert list(reversed([1, 2, 3, 4])) == [4, 3, 2, 1]

def test_some_primes():
    assert 37 in {
        num
        for num in range(2, 50)
        if not any(num % div == 0 for div in range(2, num))
    }
```

위의 코드는 일반적인 Python 함수와 매우 흡사하다. 이 모든 것은 시작하기 위해 새로운 구성을 배울 필요가 없기 때문에 `pytest`의 학습 곡선을 `unittest` 보다 더 낮게 만든다.

각 테스트는 매우 작고 자체적으로 수행된다. 이는 일반적인 현상이다. 긴 함수 이름을 볼 수 있지만 함수 내에서 많은 작업이 수행되지는 않는다. 이 기능은 주로 테스트를 서로 격리하는 역할을 하므로, 문제가 발생한 경우 문제가 발생한 위치를 정확하게 파악할 수 있다. 라벨링이 출력에서 훨씬 더 낫다는 것이 좋은 side effect이다.

메인 프로젝트와 함께 테스트 슈트를 만드는 프로젝트의 예를 보려면 [TDD로 Python에서 해시 테이블 구축 튜토리얼](https://realpython.com/python-hash-table/)을 참조하세오. 또한 [면접을 준비](https://realpython.com/python-practice-problems/)하거나 [CSV 파일을 구문 분석](https://realpython.com/python-interview-problem-parsing-csv-files/)하며, 스스로 TDD를 시도하기 위한 Python 실습 문제를 해결할 수 있다.

다음 섹션에서는 테스트 입력 값을 관리하는 데 도움을 주는 `pytest` 기능인 fixture를 살펴본다.

## 상태와 종속성의 쉬운 관리
[dictionaries](https://realpython.com/python-dicts/)이나 [JSON](https://realpython.com/python-json/) 파일과 같이 코드에서 발생할 가능성이 있는 mock 개체인 데이터 또는 [테스트 doubles](https://en.wikipedia.org/wiki/Test_double) 유형에 따라 테스트는 종종 달라진다.

`unittest`를 사용하면 이러한 종속성을 `.setUp()` 및 `.tearDown()` 메서드로 추출하여 클래스의 각 테스트가 이러한 종속성을 사용할 수 있다. 이러한 특수 방법을 사용하는 것도 좋지만, 검사 클래스가 커질수록 실수로 검정의 종속성이 전적으로 **암묵적이** 될 수 있다. 다른 말로, 여러 검정 중 하나를 분리하여 보면 다른 검정에 따라 달라진다는 것을 즉시 알 수 없다.

시간이 지남에 따라 암묵적 의존성은 복잡한 코드 엉킴을 초래할 수 있으며 테스트를 이해하기 위해 이를 풀어야 한다. 테스트는 코드를 더 이해하기 쉽게 만드는 데 도움이 될 것이다. 만약 시험 자체가 이해하기 어렵다면, 곤경에 처할 수도 있다!

`pytest`는 다른 접근 방식을 취하고 있다. 이를 통해 [fixtures](https://docs.pytest.org/en/latest/fixture.html)을 사용할 수 있는 덕분에 여전히 재사용 가능한 **명시적** 종속성 선언을 확인할 수 있다. `pytest` fixtures는 데이터 생성, 중복 테스트(test doubles) 또는 테스트 스위트를 위하여 시스템 상태를 초기화할 수 있는 함수이다. fixture를 사용하려는 모든 테스트는 이 fixture 기능을 테스트 함수의 인수로 명시적으로 사용해야 하므로 의존성을 항상 앞에 명시한다.

```python
# fixture_demo.py

import pytest

@pytest.fixture
def example_fixture():
    return 1

def test_with_fixture(example_fixture):
    assert example_fixture == 1
```

테스트 함수를 보면 전체 파일에서 fixture 정의를 확인할 필요 없이 fixture에 따라 달라진다는 것을 즉시 알 수 있다.

> **Note**: 일반적으로 프로젝트의 루트 수준에서 `test``라는 자체 폴더에 테스트를 넣으려고 한다.

> Python 애플리케이션 구성에 대한 자세한 내용은 해당 주제에 대한 [비디오 강좌](https://realpython.com/courses/structuring-python-application/)를 참조하시오.

fixtures는 다른 fixtures를 이용할 수도 있다. 다시 말하지만, 이러한 fixtures를 종속성이라고 명시적으로 선언한다. 즉, 시간이 지남에 따라 fixtures가 커지고 모듈화될 수 있다. 다른 fixtures에 fixtures를 삽입할 수 있는 기능은 엄청난 유연성을 제공하지만 테스트 스위트가 커감에 따라 종속성 관리가 더욱 어려워질 수도 있다.

이 튜토리얼의 뒷부분에서 fixtures에 대해 자세히 알아보고 이러한 문제를 해결하기 위한 몇 가지 기술을 사용해 보겠다.

## 쉬운 테스트 필터링
테스트 스위트이 커지면 기능에 대해 몇 가지 테스트만 실행하고 나중에 사용할 수 있도록 스위트 전체을 저장할 수 있다. 이를 위하여 `pytest`는 다음과 같은 몇 가지 방법을 제공한다.

- **이름 기반 필터링(name-based filtering**: `pytest`는 특정 식과 일치하는 이름의 테스트만 실행하도록 제한할 수 있다. `-k` 파라미터를 사용하여 이를 수행할 수 있다.
- **디렉토리 범위 지정(directory scoping**: 기본적으로 `pytest`는 현재 디렉토리에 있거나 현재 디렉토리 아래에 있는 테스트만을 수행한다.
- **테스트 분류(test categorizing)**: `pytest`는 사용자가 정의하는 특정 범주에서 테스트를 포함하거나 제외할 수 있다. `-m` 파라미터를 사용하여 이를 수행할 수 있다.

특히 테스트 분류는 미묘하게 강력한 도구이다. `pytest`를 사용하면 원하는 테스트를 **마크**하거나 이에 사용자 정의 레이블을 줄 수 있다. 하나의 테스트는 여러 레이블을 갖을 수 있으며, 이 레이블을 사용하여 수행할 테스트를 세부적으로 제어할 수 있다. 이 튜토리얼에서 [pytest 마크가 어떻게 작동하는지](marks.md)에 대한 예를 보이고, 대규모 테스트 스위트에서 `pytest` 마크를 사용하는 방법에 대해 설명할 것이다.

## 테스트 파라미터라이제이션(parameterization)
데이터를 처리하거나 일반적인 변환을 수행하는 기능을 테스트할 때 유사한 테스트를 많이 작성하는 자신을 발견할 수 있습니다. 테스트 코드의 [입력 또는 출력](https://realpython.com/python-input-output/)에만 차이가 있을 수 있다. 이렇게 하려면 테스트 코드를 복제해야 하며, 테스트하려는 동작이 모호해질 수 있다.

`unittest`는 여러 테스트를 하나로 수집하는 방법을 제공하지만 결과 보고서에는 개별 테스트가 출력되지 않는다. 하나의 테스트가 실패하고 나머지는 합격하더라도 그룹 전체는 여전히 단일 불합격 결과를 반환한다. `pytest`는 각 테스트가 독립적으로 통과하거나 실패할 수 있는 자체 솔루션을 제공한다. 이 튜토리얼에서 `pytest`를 사용하여 [테스트를 파라미터화하는 방법](parameterization.md)을 설명한다.

## 플러그인 기반 아키텍처
`pytest`의 가장 뒤어난 특징 중 하나는 커스터마이징과 새로운 기능에 대한 개방성이다. 프로그램의 거의 모든 부분을 열어 변경할 수 있다. 그 결과, `pytest` 사용자는 유용한 풍부한 플러그인 생태계를 개발했다.

일부 `pytest` 플러그인은 [Django](https://www.djangoproject.com/)와 같은 특정 프레임워크에 초점을 맞추었지만, 다른 것들을 대부분의 테스트 스의트에 적용할 수 있다. [일부 플러그인에 대한 자세한 내용](plugins.md)을 이 튜토리얼에서 다룰 것이다.

