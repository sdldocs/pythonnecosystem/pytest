pytest fixture는 테스트에 데이터, 반복 테스트 또는 상태 설정을 제공하는 방법이다. fixture는 광범위하게 값을 반환할 수 있는 기능이다. fixture에 의존하는 각 테스트에서는 해당 명시적으로 fixture를 인수로 받아들여야 한다.

## fixture 생성
이 섹션에서는 전형적인 [TDD(Test-Driven Development)](https://realpython.com/courses/test-driven-development-pytest/) 워크플로우를 시뮬레이션한다.

API 엔드포인트(endpoint)가 반환하는 데이터를 처리하기 위해 `format_data_for_display()` 함수를 작성하고 있다고 가정하자. 데이터는 사용자들의 이름, 성 및 직책 리스트이다. 함수는 각 사용자의 전체 이름(이름 다음 성), 콜론 그리고 직책들의 문자열 리스트을 출력해야 한다.

```python
# format_data.py

def format_data_for_display(people):
    ...  # Implement this!
```

TDD 방식으로, 테스트를 먼저 작성할 것이며, 이를 위해 다음과 같이 코드를 작성할 수 있다.

```python
# test_format_data.py

def test_format_data_for_display():
    people = [
        {
            "given_name": "Alfonsa",
            "family_name": "Ruiz",
            "title": "Senior Software Engineer",
        },
        {
            "given_name": "Sayid",
            "family_name": "Khan",
            "title": "Project Manager",
        },
    ]

    assert format_data_for_display(people) == [
        "Alfonsa Ruiz: Senior Software Engineer",
        "Sayid Khan: Project Manager",
    ]
```

이 테스트를 작성하는 동안 Excel에서 사용할 comma-separated values(CSV)로 변환하기 위해 다른 함수를 작성해야 할 수 있다.

```python
# format_data.py

def format_data_for_display(people):
    ...  # Implement this!

def format_data_for_excel(people):
    ... # Implement this!
```

할 일 목록이 늘어난다! 다행이다! TDD의 장점 중 하나는 작업을 미리 계획하는 데 도움이 된다는 점이다. `format_data_for_excel()` 함수에 대한 테스트는 `format_data_for_display()` 함수와 매우 유사하다.

```python
# test_format_data.py

def test_format_data_for_display():
    # ...

def test_format_data_for_excel():
    people = [
        {
            "given_name": "Alfonsa",
            "family_name": "Ruiz",
            "title": "Senior Software Engineer",
        },
        {
            "given_name": "Sayid",
            "family_name": "Khan",
            "title": "Project Manager",
        },
    ]

    assert format_data_for_excel(people) == """given,family,title
Alfonsa,Ruiz,Senior Software Engineer
Sayid,Khan,Project Manager
"""
```

특히, 두 테스트 모두 꽤 많은 people 변수의 정의 코드를 반복해야 한다.

모든 기본 테스트 데이터를 사용하는 여러 테스트를 작성하는 경우 fixture가 이후에 나타날 수 있다. 반복된 데이터를 함수가 `pytest` fixture임을 나타내는 @pytest.fixture로 장식된 단일 함수로 끌어낼 수 있다. 

```python
# test_format_data.py

import pytest

@pytest.fixture
def example_people_data():
    return [
        {
            "given_name": "Alfonsa",
            "family_name": "Ruiz",
            "title": "Senior Software Engineer",
        },
        {
            "given_name": "Sayid",
            "family_name": "Khan",
            "title": "Project Manager",
        },
    ]

# ...
```

함수 참조를 테스트에 인수로 추가하여 fixture를 사용할 수 있다. fixture 함수를 호출하지 않는다. `pytest`가 스스로 fixture 함수의 이름으로 fixture 함수 반환 값을 사용할 수 있다.

```python
# test_format_data.py

# ...

def test_format_data_for_display(example_people_data):
    assert format_data_for_display(example_people_data) == [
        "Alfonsa Ruiz: Senior Software Engineer",
        "Sayid Khan: Project Manager",
    ]

def test_format_data_for_excel(example_people_data):
    assert format_data_for_excel(example_people_data) == """given,family,title
Alfonsa,Ruiz,Senior Software Engineer
Sayid,Khan,Project Manager
"""
```

각 테스트는 이제 현저하게 짧아졌지만 여전히 종속된 데이터에 대한 명확한 경로를 가지고 있다. fixture 이름을 구체적으로 지정하시오. 그렇게 하면, 새로운 테스트를 작성할 때 그것을 사용할지 여부를 빠르게 결정할 수 있다!

fixture의 파워을 처음 발견했을 때 항상 사용하는 것이 유혹적일 수 있지만, 모든 것과 마찬가지로 균형을 유지해야 한다.

## fixture 사용 지양
fixture는 여러 테스트에서 사용하는 데이터 또는 객체를 추출하는 데 유용하다. 그러나 약간의 데이터 변경이 필요한 테스트에 항상 좋은 것은 아니다. 테스트 스위트에 fixture를 제거하는 것이 일반 데이터나 객체를 제가하는 것보다 결코 바람직하지 않다. 그것은 간접 층이 추가되었기 때문에 더 나빠질 수 있기 째문이다.

대부분의 추상화와 마찬가지로, 적절한 수준의 fixture 사용을 찾기 위해서는 약간의 연습과 생각이 필요하다.

그럼에도 불구하고, fixture는 테스트 스위트의 필수적인 부분이 될 수 있다. 프로젝트의 범위가 넓어짐에 따라 규모의 문제가 현실로 나타나기 시작한다. 모든 종류의 툴이 직면한 문제 중 하나는 규모에 맞게 사용되도록 처리하는 방법이며, 다행히도 `pytest`에는 성장에 따른 복잡성을 관리하는 데 도움이 되는 유용한 많은 기능을 지원하고 있다.

## 규모에 적합한 fixture 사용 
테스트에서 더 많은 fixture를 추출할수록 일부 fixture로 부터 추가 추상화의 이점을 얻을 수 있다는 것을 알 수 있다. `pytest`에서 fixture를 **모듈화**할 수 있다. 모듈화는 fixture가 import될 수 있고, 다른 모듈에서 fixture를  import할 수 있으며, 다른 fixture에 의존하여 import할 수 있음을 뜻한다. 이 모든 것을 통해 사용 사례에 적합한 fixture 추상화를 구성할 수 있다.

예를 들어, 두 개의 개별 파일 또는 모듈의 fixture가 공통 종속성을 공유한다는 것을 발견할 수 있다. 이 경우 테스트 모듈에서 보다 일반적인 fixture 관련 모듈로 fixture를 이동할 수 있다. 이렇게 하면 필요한 모든 테스트 모듈을 다시 import할 수 있다. 이 방법은 프로젝트 내내 fixture를 반복적으로 사용하는 경우에 적합하다.
