
앞서 `pytest` fixture를 사용하여 공통 종속성을 추출하여 코드 중복을 줄이는 방법을 살펴보았습니다. 입력과 예상 출력이 약간 다른 여러 테스트가 있는 경우 fixture는 그다지 유용하지 않습니다. 이 경우 단일 테스트 선언을 **파라미터화**할 수 있으며, `pytest`는 사용자가 지정한 파라미터를 사용하여 테스트의 변형을 만든다.

문자열이 [palindrome](https://en.wikipedia.org/wiki/Palindrome)인지 확인하는 함수를 작성했다고 가정해 보자. 초기 테스트 세트는 다음과 같을 수 있다.

```python
def test_is_palindrome_empty_string():
    assert is_palindrome("")

def test_is_palindrome_single_character():
    assert is_palindrome("a")

def test_is_palindrome_mixed_casing():
    assert is_palindrome("Bob")

def test_is_palindrome_with_spaces():
    assert is_palindrome("Never odd or even")

def test_is_palindrome_with_punctuation():
    assert is_palindrome("Do geese see God?")

def test_is_palindrome_not_palindrome():
    assert not is_palindrome("abc")

def test_is_palindrome_not_quite():
    assert not is_palindrome("abab")
```

마지막 두 테스트를 제외한 모든 테스트는 동일한 형태를 갖고 있다.

```python
def test_is_palindrome_<in some situation>():
    assert is_palindrome("<some string>")
```

표준 형식 냄새가 많이 난다. `pytest `는 지금까지 표준 형식을 제거하는데 도움을 줬고, 지금 실망시키진 않을 거이다. `@pytest.mark.parametrize`를 사용하여 이 패턴에 다른 값으로 채울 수 있으므로 테스트 코드를 크게 줄일 수 있다.

```python
@pytest.mark.parametrize("palindrome", [
    "",
    "a",
    "Bob",
    "Never odd or even",
    "Do geese see God?",
])
def test_is_palindrome(palindrome):
    assert is_palindrome(palindrome)

@pytest.mark.parametrize("non_palindrome", [
    "abc",
    "abab",
])
def test_is_palindrome_not_palindrome(non_palindrome):
    assert not is_palindrome(non_palindrome)
```

parametrize()의 첫 번째 인수는 쉼표로 구분된 파라미터 이름의 문자열입니다. 위의 예에서 볼 수 있듯이 두 개 이상의 이름을 제공할 필요가 없다. 두 번째 인수는 매개 변수 값을 나타내는 [튜플](https://realpython.com/python-lists-tuples/#python-tuples) 또는 단일 값의 [리스트](https://realpython.com/python-lists-tuples/#python-lists)이다. 파라미터화를 한 단계 더 진행하여 모든 테스트를 하나로 통합할 수 있다.

```python
@pytest.mark.parametrize("maybe_palindrome, expected_result", [
    ("", True),
    ("a", True),
    ("Bob", True),
    ("Never odd or even", True),
    ("Do geese see God?", True),
    ("abc", False),
    ("abab", False),
])
def test_is_palindrome(maybe_palindrome, expected_result):
    assert is_palindrome(maybe_palindrome) == expected_result
```

이렇게 하면 코드가 단축되지만 이 경우 원래 함수의 보다 설명적인 특성을 잃을 수 있다. 테스트 스위트를 이해하기 어렵도록 파라메타화하지 않도록 하지 않는다. 파라미터화를 사용하여 테스트 동작에서 테스트 데이터를 분리하여 테스트의 대상을 명확하게 하고 다양한 테스트 사례를 쉽게 읽고 유지 관리할 수 있도록 한다.
